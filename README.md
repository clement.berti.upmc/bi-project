# Projet BI - Twitter trends

Bienvenue sur les fichiers sources de notre projet portant sur l'étude des trends Twitter.

## Les fichiers

Vous trouverez ici:
- le rapport
- les slides
- les fichiers Pentaho sous `/fichiers-pentaho`
- les fichiers Pentaho server sous `/fichiers-bi-server`
- les fichiers Rapidminer sous `/rapidminer`
- la base de donnée utilisé pour bi-server sous `/fichiers-bi-server/bi_project_dw.sql`
- la base de donnée utilisé pour rapidminer sous `/data/ALLxCATEGORIES_100.csv`
- les différentes bases et fichiers pour pentaho sous `/data/all.csv` et `/data/world.sql`
- les scripts pour extraire les données Twitter dans `/extraction`

## Note

Une partie du code est dupliqué dans plusieurs fichiers Pentaho. Ceci est le seul moyen que nous avons trouvé pour remplir la table. Nous ne parvenions pas a importer le fichier `/fichiers-pentaho/fetch_tag.ktr` dans les transformations...

Cordialement,

Clément BERTI & Pierre SIMON
