#-*- coding:utf-8 -*-
import sys
import time
import pickle
import csv
import ast
''' paramètre 1 : fichier de trends avec localisation et timestamp, paramètre 2 : fichier de trends avec catégorie '''

def normaliser(trend):
    return trend[(0 if trend[0]!='#' else 1):].lower().replace(' ', '').replace('-', '').replace("'", '').replace("\\", '')

def sauvegarder_dictionnaire(dictionnaire):
    with open('TRENDSxCAT.csv', 'w+') as f:
        writer = csv.writer(f)
        for key, value in dictionnaire.items():
            for key2, value2 in value.items():
                writer.writerow([key, key2, value2[0], value2[1], value2[2], value2[3]])

entree = sys.argv[2]
with open(entree) as f:
    reader = csv.reader(f, delimiter=',')
    dictionnaire = {el[0]:[el[1] if el[1] != '' else None, ast.literal_eval(el[2]) if (el[2] != '' and el[2] != 'set()') else None] for el in reader}
    '''
    dictionnaire = {}
    for el in reader:
        print(el)
        dictionnaire[el[0]] = [el[1], ast.literal_eval(el[2]) if (el[2] != '' and el[2] != 'set()') else None]
    '''
#print(dictionnaire) 

entree = sys.argv[1]
localisations = {}
with open(entree) as f:
    reader = csv.reader(f, delimiter=';')
    for trend in reader:
        if trend[0] not in localisations:
            localisations[trend[0]] = {}
        trend_n = normaliser(trend[2])
        if trend_n not in localisations[trend[0]]:
            #print("trend non présente")
            localisations[trend[0]][trend_n] = [trend[1], trend[1], trend[3], dictionnaire[trend_n][1] if trend_n in dictionnaire else None]
        else:
            localisations[trend[0]][trend_n][1] = trend[1]
            if trend[3] > localisations[trend[0]][trend_n][2]:
                #print("trouvée une trend dupliquée avec un volum sup")
                localisations[trend[0]][trend_n][2] = trend[3]

#print(localisations)
sauvegarder_dictionnaire(localisations)
