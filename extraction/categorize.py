#-*- coding:utf-8 -*-
import sys
import time
import pickle
import csv
import json
import urllib
import urllib.parse
import urllib.request
''' Fichier incomplet, utiliser categorize_simpler.py à la place '''
def normaliser(trend):
    return trend[(0 if trend[0]!='#' else 1):].lower().replace(' ', '').replace('-', '').replace("'", '')

def interroger_kg(query):
    params['query'] = query
    url = service_url + '?' + urllib.parse.urlencode(params)
    response = None
    while response is None:
        try:
            response = json.loads(urllib.request.urlopen(url).read())
        except:
            print("Erreur de connexion à l'api ! Nouvelle tentative dans 5 secondes…")
            time.sleep(5)
    return response

def sauvegarder_dictionnaire(dictionnaire):
    with open('CATEGORIES.csv', 'w+') as f:
        writer = csv.writer(f)
        for key, value in dictionnaire.items():
            writer.writerow([key, value[0], value[1], value[2], value[3]])

api_key = open('google_kg.api_key').read()
service_url = 'https://kgsearch.googleapis.com/v1/entities:search'
params = {
    'query': None,
    'languages': 'en',
    'limit': 3,
    'indent': True,
    'key': api_key,
}

adjectif_localisation = {'United-States':'american', 'India':'indian', 'Indonesia':'indonesian', 'Japan':'japanese', 'Brazil':'brazilian', 'United-Kingdom':'british', 'Mexico':'mexican', 'Russia':'russian', 'Philippines':'filipino', 'Spain':'spanish', 'Canada':'canadian', 'Australia':'australian', 'Germany':'german', 'France':'french', 'Netherlands':'dutch', 'South-Africa':'south african', 'Turkey':'turkish', 'South-Korea':'south korean', 'Italy':'italian', 'Venezuela':'venezuelan', 'Ukraine':'ukrainian', 'Vietnam':'vietnamese', 'Poland':'polish', 'Saudi-Arabia':'saudi arabian', 'Worldwide':'ABCDEFGHIJK', 'San-Francisco':'american', 'Los-Angeles':'american', 'New-York':'american', 'Atlanta':'american', 'Mumbai':'indian', 'Bangalore':'indian', 'Jaipur':'indian', 'Jakarta':'indonesian','Bandung':'indonesian', 'Makassar':'indonesian', 'Tokyo':'japanese', 'Sapporo':'japanese', 'Fukuoka':'japanese', 'Rio-de-Janeiro':'brazilian', 'São-Paulo':'brazilian', 'Brasília':'brazilian', 'London':'british', 'Cardiff':'british', 'Edimburgh':'british', 'Liverpool':'british', 'Mexico-City':'mexican', 'Guadalajara':'mexican', 'Monterrey':'mexican', 'Moscow':'russian', 'Saint-Petersburg':'russian', 'Perm':'russian', 'Manila':'filipino', 'Cebu-City':'filipino', 'Makati':'filipino', 'Madrid':'spanish', 'Barcelona':'spanish', 'Seville':'spanish', 'Ottawa':'canadian', 'Montreal':'canadian', 'Vancouver':'canadian', 'Sydney':'australian', 'Melbourne':'australian', 'Perth':'australian', 'Berlin':'german', 'Munich':'german', 'Hamburg':'german', 'Paris':'french', 'Marseille':'french', 'Bordeaux':'french', 'Amsterdam':'dutch', 'Rotterdam':'dutch', 'Utrecht':'dutch', 'Johannesburg':'south african', 'Cape-Town':'south african', 'Pretoria':'south african', 'Ankara':'turkish', 'Istanbul':'turkish', 'Izmir':'turkish', 'Seoul':'south korean', 'Busan':'south korean', 'Gwangju':'south korean', 'Rome':'italian', 'Milan':'italian', 'Palermo':'italian', 'Caracas':'venezuelan', 'Valencia':'venezuelan', 'Maracaibo':'venezuelan', 'Kyiv':'ukrainian', 'Lviv':'ukrainian', 'Odesa':'ukrainian', 'Hanoi':'vietnamese', 'Ho-Chi-Minh-City':'vietnamese', 'Da-Nang':'vietnamese', 'Warsaw':'polish', 'Kraków':'polish', 'Poznań':'polish', 'Riyadh':'saudi arabian', 'Mecca':'saudi arabian', 'Dammam':'saudi arabian' }

dictionnaire = {}
# format : <trend_normalisée>:[[trend_a, trend_b, trend_c, …], trend_canonique, ['catégorie_a', 'catégorie_b'], pays_dans_description?, 'pays concerné']
entree = sys.argv[1]#'data/ALL_50000.csv'
with open(entree, newline='') as f:
    reader = csv.reader(f, delimiter=';')
    for i, line in enumerate(reader):
        if i % 100 == 0:
            print("Ligne :", i)
        trend = line[2]
        loc = line[0]
        trend_n = normaliser(trend)
        if trend_n not in dictionnaire:
            response = interroger_kg(trend)
            if len(response['itemListElement']) == 0:
                dictionnaire[trend_n] = [[trend], None, None, False, 'none']
            else:
                #print(response['itemListElement'][0]['result'])
                paysDansDescription = False
                if 'detailedDescription' in response['itemListElement'][0]['result'] and adjectif_localisation[loc] in response['itemListElement'][0]['result']['detailedDescription']['articleBody'].lower():
                    paysDansDescription = True
                dictionnaire[trend_n] = [[trend], trend, set(response['itemListElement'][0]['result']['@type']) - {'Thing'}, paysDansDescription, 'none']
        elif trend not in dictionnaire[trend_n][0]:
            dictionnaire[trend_n][0].append(trend)
            response = interroger_kg(trend)
            if len(response['itemListElement']) != 0:
                paysDansDescription = False
                if 'detailedDescription' in response['itemListElement'][0]['result'] and adjectif_localisation[loc] in response['itemListElement'][0]['result']['detailedDescription']['articleBody'].lower():
                    paysDansDescription = True
                if dictionnaire[trend_n][2] == None:
                    dictionnaire[trend_n][1] = trend
                    dictionnaire[trend_n][2] = set(response['itemListElement'][0]['result']['@type']) - {'Thing'}
                    dictionnaire[trend_n][3] = True
                elif set(response['itemListElement'][0]['result']['@type']) - {'Thing'} != dictionnaire[trend_n][2] and dictionnaire[trend_n][3] == False and paysDansDescription == True: 
                    print("Catégorisation de", trend)
                    print("Catégories actuelles :", dictionnaire[trend_n][2])
                    print("Nouvelles catégories :", set(response['itemListElement'][0]['result']['@type']) - {'Thing'})
                    dictionnaire[trend_n][1] = trend
                    dictionnaire[trend_n][2] = set(response['itemListElement'][0]['result']['@type']) - {'Thing'}
                    dictionnaire[trend_n][3] = True

sauvegarder_dictionnaire(dictionnaire)
