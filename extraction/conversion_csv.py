# -*- coding: utf-8 -*-
import sys
import numpy as np
# arg1 : fichier source, arg2 : fichier cible, arg3 : localisation
filename = str(sys.argv[1])
#print(filename)
array = []
location = str(sys.argv[3])
current_date = None
current_trend = None
current_volume = None
position = None
with open(filename, 'r') as f:
    line = f.readline()
    while(line != ''):
        line = f.readline()
        #print(line, end='')
        #if(position != None):
        #    if(line[0:2])
        #    continue
        if(line[0:2] == '0_'):
            current_date = line[2:-1]
        elif(line[0:2] == '1_' and position == None):
            current_trend = line[2:-1]
        elif(line[0:2] == '2_' and position == None):
            current_volume = line[2:-2]
        elif(line[0:2] == ']['):
            current_date = None
            current_trend = None
            current_volume = None
        else:
            continue
        if(current_date != None and current_trend != None and current_volume != None):
            array.append([location, current_date, current_trend, current_volume])
            current_trend = None
            current_volume = None
            if(position != None):
                #print("Retour en arrière!")
                f.seek(position)
                position = None
        elif(current_date == None and current_trend != None and current_volume != None and position == None):
            #print("Recherche de la date…")
            position = f.tell()

nparray = np.array(array, dtype='str')
with open(str(sys.argv[2]), 'a') as f:
    np.savetxt(f, nparray, delimiter=";", fmt="%s")

