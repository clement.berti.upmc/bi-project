#sed -n '/"url"/!p' France_0 | sed -n '/"query"/!p' | sed -n '/"promoted_content"/!p' | sed -n '/"created_at"/!p'

DATA_PATH='locations'
TEMP_PATH='extraction_temp'
OUTPUT_PATH='output'

for f in $DATA_PATH/*
do
  echo "extraction des données de : "$(basename $f)
  sed '/"url"/d' $f |
    sed '/"query"/d' |
    sed '/"promoted_content"/d' |
    #sed '/"created_at"/d' |
    sed '/"as_of"/d' |
    sed '/locations/,+5d' |
    sed '/null/{$!{N;/null,\n    "name"/d}}' |
    sed '/name/{$!{N;/",\n    "tweet_volume": null/d}}' |
    sed '/{/{$!{N;/{\n   }/d}}' |
    sed '/{/d' | sed '/}/d' |
    sed '/ \[/d' | sed '/\],/d' |
    sed 's/: /:/g' |
    sed 's/  //g' |
    sed 's/"tweet_volume":/2_/g' |
    sed 's/"name":/1_/g' |
    #sed 's/"as_of":/0_/g' |
    sed 's/"created_at":/0_/g' |
    sed 's/",//g' | sed 's/"//g' |
    sed 's/,//g' > $TEMP_PATH/extraits_$(basename $f)
  echo "conversion csv…"
  python3 conversion_csv.py $TEMP_PATH/extraits_$(basename $f) $OUTPUT_PATH/ALL.csv $(basename $f) &> $OUTPUT_PATH/extraction_log.txt
  #$TEMP_PATH/$(basename $f)".csv" $(basename $f)
done
