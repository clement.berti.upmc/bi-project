#!/usr/bin/python
# -*- coding: UTF-8 -*-

import json
import time
import os
from twitter import Twitter, OAuth, TwitterHTTPError, TwitterStream

ACCESS_TOKEN = 'TOKEN1'
ACCESS_SECRET = 'SECRET1'
CONSUMER_KEY = 'CTOKEN1'
CONSUMER_SECRET = 'CSECRET1'

ACCESS_TOKEN_2 = 'TOKEN2'
ACCESS_SECRET_2 = 'SECRET2'
CONSUMER_KEY_2 = 'CTOKEN2'
CONSUMER_SECRET_2 = 'CSECRET2'

dict_pays = {'United-States':23424977, 'India':23424848, 'Indonesia':23424846, 'Japan':23424856, 'Brazil':23424768,
             'United-Kingdom':23424975, 'Mexico':23424900, 'Russia':23424936, 'Philippines':23424934, 'Spain':23424950,
             'Canada':23424775, 'Australia':23424748, 'Germany':23424829, 'France':23424819, 'Netherlands':23424909,
             'South-Africa':23424942, 'Turkey':23424969, 'South-Korea':23424868, 'Italy':23424853, 'Venezuela':23424982,
             'Ukraine':23424976, 'Vietnam':23424984, 'Poland':23424923, 'Saudi-Arabia':23424938, 'Worldwide':1}

dict_villes = {
    'San-Francisco': 2514815,   'Los-Angeles': 2442047,         'New-York': 2459115,    'Atlanta': 2357024,
    'Mumbai': 2295411,          'Bangalore': 2295420,           'Jaipur': 2295401,
    'Jakarta': 1047378,         'Bandung': 1047180,             'Makassar': 1046138,
    'Tokyo': 1118370,           'Sapporo': 1118108,             'Fukuoka': 1117099,
    'Rio-de-Janeiro': 455825,   'São-Paulo': 455827,            'Brasília': 455819,
    'London': 44418,            'Cardiff': 15127,               'Edimburgh': 19344,     'Liverpool': 26734,
    'Mexico-City': 116545,      'Guadalajara': 124162,          'Monterrey':134047,
    'Moscow': 2122265,          'Saint-Petersburg': 2123260,    'Perm': 2122814,
    'Manila': 1199477,          'Cebu-City': 1199079,           'Makati': 1180689,
    'Madrid': 766273,           'Barcelona': 395273,            'Seville': 774508,
    'Ottawa': 3369,             'Montreal': 3534,               'Vancouver': 9807,
    'Sydney': 1105779,          'Melbourne': 1103816,           'Perth': 1098081,
    'Berlin': 638242,           'Munich': 676757,               'Hamburg': 656958,
    'Paris': 615702,            'Marseille': 610264,            'Bordeaux': 580778,
    'Amsterdam': 727232,        'Rotterdam': 733075,            'Utrecht': 734047,
    'Johannesburg': 1582504,    'Cape-Town': 1591691,           'Pretoria': 1586638,
    'Ankara': 2343732,          'Istanbul': 2344116,            'Izmir': 2344117,
    'Seoul': 1132599,           'Busan': 1132447,               'Gwangju': 1132481,
    'Rome': 721943,             'Milan': 718345,                'Palermo': 719846,
    'Caracas':395269,           'Valencia': 395272,             'Maracaibo': 395270,
    'Kyiv': 924938,             'Lviv': 924943,                 'Odesa': 929398,
    'Hanoi': 1236594,           'Ho-Chi-Minh-City': 1252431,    'Da-Nang': 1252376,
    'Warsaw': 523920,           'Kraków': 502075,               'Poznań': 514048,
    'Riyadh': 1939753,          'Mecca': 1939897,               'Dammam':1939574
}

DELAY = 300 # secondes
# on peut faire maximum 75 requêtes toutes les 15 minutes
# donc pour 25 pays on prend un délai de 5 minutes = 300 secondes.
# donc pour 74 villes on prend un délai de 900 secondes.

# apparemment Twitter génère un nouveau json de tendance toutes les 5 minutes à XXhX{0,5}min~40sec

oauth = OAuth(ACCESS_TOKEN, ACCESS_SECRET, CONSUMER_KEY, CONSUMER_SECRET)
twitter = Twitter(auth=oauth)

oauth_2 = OAuth(ACCESS_TOKEN_2, ACCESS_SECRET_2, CONSUMER_KEY_2, CONSUMER_SECRET_2)
twitter_2 = Twitter(auth=oauth_2)

if not os.path.exists('data'):
    os.makedirs('data')

DATA_PATH = ''

i = 0
while(1):
    print('Iter:', i)
    if i%3 == 0:
        for key, value in dict_villes.items():
            json_temp = twitter_2.trends.place(_id = value)
            with open(DATA_PATH+key, 'a') as file:
                json.dump(json_temp, file, indent=1, ensure_ascii=False)
        print(json.dumps(twitter_2.application.rate_limit_status()['resources']['trends']['/trends/place'], indent=4))

    for key, value in dict_pays.items():
        json_temp = twitter.trends.place(_id = value)
        #with open(DATA_PATH+key+'_'+json_temp[0]['as_of'], 'w') as file:
        with open(DATA_PATH+key, 'a') as file:
            json.dump(json_temp, file, indent=1, ensure_ascii=False)
    print(json.dumps(twitter.application.rate_limit_status()['resources']['trends']['/trends/place'], indent=4))
    time.sleep(DELAY)
    i += 1
