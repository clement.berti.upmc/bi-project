#-*- coding:utf-8 -*-
import sys
import time
import pickle
import csv
import json
import urllib
import urllib.parse
import urllib.request
''' le fichier en entrée doit avoir uniquement une trend par ligne '''
def normaliser(trend):
    return trend[(0 if trend[0]!='#' else 1):].lower().replace(' ', '').replace('-', '').replace("'", '')

def interroger_kg(query):
    params['query'] = query
    url = service_url + '?' + urllib.parse.urlencode(params)
    response = None
    while response is None:
        try:
            response = json.loads(urllib.request.urlopen(url).read())
        except:
            print("Erreur de connexion à l'api ! Nouvelle tentative dans 5 secondes…")
            time.sleep(5)
    return response

def sauvegarder_dictionnaire(dictionnaire):
    with open('CATEGORIES.csv', 'w+') as f:
        writer = csv.writer(f)
        for key, value in dictionnaire.items():
            writer.writerow([key, value[1], value[2]])

api_key = open('google_kg.api_key').read()
service_url = 'https://kgsearch.googleapis.com/v1/entities:search'
params = {
    'query': None,
    'languages': 'en',
    'limit': 3,
    'indent': True,
    'key': api_key,
}

entree = sys.argv[1]#'data/TRENDS_100.csv'
entrees_uniques = {}
start = time.time()

with open(entree) as f:
    entrees = len(f.readlines())
print("Nombre d'entrées :", entrees)

with open(entree) as f:
    entrees_uniques = {el if el[0] != '#' else el[1:] for el in f.read().splitlines()}
    #entrees_uniques = set(f.read().splitlines())
stop = time.time()
print("Temps écoulé :", stop-start)
print("En ne comptant qu'une seule fois les trends identiques et en supprimant les croisillons, réduction de :", 100*(entrees-len(entrees_uniques))/entrees, "%. Nombre de trends :", len(entrees_uniques))
#print(entrees_uniques)


dictionnaire = {}
# format : <trend_normalisée>:[[trends_deja_testees…], trend_canonique, ['catégorie_a', 'catégorie_b']]
for i, trend in enumerate(entrees_uniques):
    if i % 100 == 0:
        print("Entree n°", i)
    trend_n = normaliser(trend)
    if trend_n not in dictionnaire:
        response = interroger_kg(trend)
        if len(response['itemListElement']) == 0 or 'result' not in response['itemListElement'][0] or '@type' not in response['itemListElement'][0]['result']:
            dictionnaire[trend_n] = [[trend], None, None]
        else:
            #print(response['itemListElement'][0]['result'])
            dictionnaire[trend_n] = [[trend], trend, set(response['itemListElement'][0]['result']['@type']) - {'Thing'}]
    elif trend not in dictionnaire[trend_n][0]:
        dictionnaire[trend_n][0].append(trend)
        response = interroger_kg(trend)
        if len(response['itemListElement']) != 0 and 'result' in response['itemListElement'][0] and '@type' in response['itemListElement'][0]['result']:
            if dictionnaire[trend_n][2] == None:
                dictionnaire[trend_n][1] = trend
                dictionnaire[trend_n][2] = set(response['itemListElement'][0]['result']['@type']) - {'Thing'}
            elif set(response['itemListElement'][0]['result']['@type']) - {'Thing'} != dictionnaire[trend_n][2]:
                print("Conflit pour la catégorisation de", trend)
                print("Catégories actuelles :", dictionnaire[trend_n][2])
                print("Nouvelles catégories :", set(response['itemListElement'][0]['result']['@type']) - {'Thing'})
                #dictionnaire[trend_n][1] = trend
                #dictionnaire[trend_n][2] = set(response['itemListElement'][0]['result']['@type']) - {'Thing'}

sauvegarder_dictionnaire(dictionnaire)


