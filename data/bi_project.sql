-- MySQL
--
-- Host: 127.0.0.1    Database: bi_project
-- ------------------------------------------------------
-- Server version       5.1.51-debug-log

DROP SCHEMA IF EXISTS bi_project;
CREATE SCHEMA bi_project;
USE bi_project;
SET AUTOCOMMIT=0;

-- SHOW ENGINE INNODB STATUS;
--
-- Table structure for table `dim_date`
--

DROP TABLE IF EXISTS `dim_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dim_date` (
  `date_key` int(8) NOT NULL,
  `date_value` date NOT NULL,
  `date_short` char(12) NOT NULL,
  `date_medium` char(16) NOT NULL,
  `date_long` char(24) NOT NULL,
  `date_full` char(32) NOT NULL,
  `day_in_year` smallint(5) NOT NULL,
  `day_in_month` tinyint(3) NOT NULL,
  `is_first_day_in_month` char(10) NOT NULL,
  `is_last_day_in_month` char(10) NOT NULL,
  `day_abbreviation` char(3) NOT NULL,
  `day_name` char(12) NOT NULL,
  `week_in_year` tinyint(3) NOT NULL,
  `week_in_month` tinyint(3) NOT NULL,
  `is_first_day_in_week` char(10) NOT NULL,
  `is_last_day_in_week` char(10) NOT NULL,
  `month_number` tinyint(3) NOT NULL,
  `month_abbreviation` char(3) NOT NULL,
  `month_name` char(12) NOT NULL,
  `year2` char(2) NOT NULL,
  `year4` smallint(5) NOT NULL,
  `quarter_name` char(2) NOT NULL,
  `quarter_number` tinyint(3) NOT NULL,
  `year_quarter` char(7) NOT NULL,
  `year_month_number` char(7) NOT NULL,
  `year_month_abbreviation` char(8) NOT NULL,
  PRIMARY KEY (`date_key`),
  UNIQUE KEY `date` (`date_value`) USING BTREE,
  UNIQUE KEY `date_value` (`date_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/* éventuellement essayer avec CHARSET=utf8mb4, mais ce n'était pas suffisant pour gérer les caractères non latin */
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dim_time`
--

DROP TABLE IF EXISTS `dim_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dim_time` (
  `time_key` int(8) NOT NULL,
  `time_value` time NOT NULL,
  `hours24` tinyint(3) NOT NULL,
  `hours12` tinyint(3) DEFAULT NULL,
  `minutes` tinyint(3) DEFAULT NULL,
  `am_pm` char(3) DEFAULT NULL,
  PRIMARY KEY (`time_key`),
  UNIQUE KEY `time_value` (`time_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dim_country`
--

DROP TABLE IF EXISTS `dim_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dim_country` (
  `code` CHAR(3) NOT NULL DEFAULT '',
  `name` CHAR(52) NOT NULL DEFAULT '',
  `continent` CHAR(52) NOT NULL DEFAULT '',
  `population` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dim_city`
--

DROP TABLE IF EXISTS `dim_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dim_city` (
  `city_code` INT(11) NOT NULL AUTO_INCREMENT,
  `country_code` CHAR(3) NOT NULL DEFAULT '',
  `name` CHAR(35) NOT NULL DEFAULT '',
  `population` INT(11) NOT NULL DEFAULT '0',
  `capital` boolean DEFAULT false,
  PRIMARY KEY (`city_code`),
  KEY `country_code` (`country_code`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`country_code`) REFERENCES `dim_country` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4080 DEFAULT CHARSET=latin1;

--
-- Table structure for table `dim_tag`
--

DROP TABLE IF EXISTS `dim_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dim_tag` (
  `tag_id` CHAR(52) NOT NULL DEFAULT '',
  `tag` CHAR(52) NOT NULL DEFAULT '',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4080 DEFAULT CHARSET=latin1;

--
-- Table structure for table `fact_trend`
--

DROP TABLE IF EXISTS `fact_trend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fact_trend` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `volume` INT(11) NOT NULL DEFAULT '0',
  `tag_id` CHAR(52) NOT NULL DEFAULT '',
  `time_key` int(8) NOT NULL,
  `date_key` int(8) NOT NULL,
  `country_code` CHAR(3),
  `city_code` INT(11),
  PRIMARY KEY (`id`),
  KEY `city_code` (`city_code`),
  KEY `country_code` (`country_code`),
  KEY `time_key` (`time_key`),
  KEY `date_key` (`date_key`),
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `hash_ibfk_1` FOREIGN KEY (`city_code`) REFERENCES `dim_city` (`city_code`),
  CONSTRAINT `hash_ibfk_2` FOREIGN KEY (`country_code`) REFERENCES `dim_country` (`code`),
  CONSTRAINT `hash_ibfk_3` FOREIGN KEY (`time_key`) REFERENCES `dim_time` (`time_key`),
  CONSTRAINT `hash_ibfk_4` FOREIGN KEY (`date_key`) REFERENCES `dim_date` (`date_key`),
  CONSTRAINT `hash_ibfk_5` FOREIGN KEY (`tag_id`) REFERENCES `dim_tag` (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4080 DEFAULT CHARSET=latin1;
